module.exports = (err, request, response, next) => {
  const status = err.status || 500;
  const message = err.message || "Server error, try later";
  response.status(status).json({ message: message });
};
