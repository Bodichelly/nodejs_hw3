module.exports = async (request, response, next) => {
  const user = request.user;
  if (user.role == "SHIPPER") {
    next();
  } else {
    return response.status(403).json({
      message: "This request is forbidden",
    });
  }
};
