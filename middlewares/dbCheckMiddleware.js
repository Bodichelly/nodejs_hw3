const auth = require("../config/auth");

const mongoose = require("mongoose");

module.exports = (request, response, next) => {
  if (mongoose.connection.readyState == 1) {
    next();
  } else {
    return response.status(500).json({ message: "DB connection problem" });
  }
};
