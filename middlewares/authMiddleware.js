const auth = require("../config/auth");
const {
  getUserByEmailAndPassword,
  getUserByEmail,
  getUser,
} = require("../dbcontroller/userDBController");
const jwt = require("jsonwebtoken");

const { secret } = require("../config/auth");

module.exports = async (request, response, next) => {
  const authHeader = request.headers["authorization"];

  if (!authHeader) {
    return response
      .status(401)
      .json({ message: "No authorization header found" });
  }

  const [, jwtToken] = authHeader.split(" ");

  try {
    const credentials = jwt.verify(jwtToken, secret);
    const user = await getUser(credentials.id);
    request.user = user;
    next();
  } catch (err) {
    return response
      .status(err.status || 401)
      .json({ message: err.message || "Invalid JWT" });
  }
};
