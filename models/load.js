const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = mongoose.model(
  "load",
  new Schema({
    created_by: {
      required: true,
      type: String,
    },
    assigned_to: {
      required: false,
      type: String,
    },
    status: {
      required: true,
      enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
      type: String,
    },
    state: {
      enum: [
        null,
        "En route to Pick Up",
        "Arrived to Pick Up",
        "En route to delivery",
        "Arrived to delivery",
      ],
      type: String,
    },
    name: {
      required: true,
      type: String,
    },
    payload: {
      required: true,
      type: Number,
    },
    pickup_address: {
      required: true,
      type: String,
    },
    delivery_address: {
      required: true,
      type: String,
    },
    dimensions: {
      width: {
        required: true,
        type: Number,
      },
      length: {
        required: true,
        type: Number,
      },
      height: {
        required: true,
        type: Number,
      },
    },
    logs: [
      {
        message: {
          required: true,
          type: String,
        },
        time: {
          required: true,
          type: String,
          format: Date,
          default: Date.now(),
        },
      },
    ],
    created_date: {
      required: true,
      type: String,
      format: Date,
      default: Date.now(),
    },
  })
);
