const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = mongoose.model(
  "truck",
  new Schema({
    created_by: {
      required: true,
      type: String,
    },
    assigned_to: {
      type: String,
    },
    type: {
      required: true,
      enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
      type: String,
    },
    status: {
      required: false,
      enum: ["OS", "OL", "IS"],
      type: String,
    },
    created_date: {
      required: true,
      type: String,
      format: Date,
      default: Date.now(),
    },
  })
);
