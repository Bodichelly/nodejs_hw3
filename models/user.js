const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = mongoose.model(
  "user",
  new Schema({
    email: {
      required: true,
      type: String,
      unique: true,
    },
    password: {
      required: true,
      type: String,
    },
    created_date: {
      required: true,
      type: String,
      format: Date,
      default: Date.now(),
    },
    role: {
      required: true,
      enum: ["SHIPPER", "DRIVER"],
      type: String,
    },
  })
);
