const typesArr = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];

const loadSizesAndTruckTypes = [
  {
    type: 0,
    dimensions: [400, 250, 200],
  },
  {
    type: 1,
    dimensions: [600, 400, 270],
  },
  {
    type: 2,
    dimensions: [900, 900, 900],
  },
];

module.exports.defineTruck = (dimensions) => {
  const loadSizes = [
    dimensions.width,
    dimensions.length,
    dimensions.height,
  ].sort((a, b) => {
    return b - a;
  });
  let currentType = 0;
  let i = 0;
  while (i < loadSizes.length) {
    if (loadSizesAndTruckTypes[currentType].dimensions[i] >= loadSizes[i]) {
      i++;
    } else {
      currentType++;
    }
  }
  return typesArr[currentType] || typesArr[2];
};
