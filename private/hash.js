const CryptoJS = require("crypto-js");
const config = require("../config/auth");

module.exports.getPasswordHashWithDefaultSalt = (password) => {
  try {
    return CryptoJS.HmacMD5(password, config.salt).toString();
  } catch (e) {
    throw {
      status: 500,
      message: "Server error",
    };
  }
};
