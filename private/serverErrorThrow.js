module.exports = (e) => {
  throw {
    status: 500,
    message: e.message || "Error while getting data",
  };
};
