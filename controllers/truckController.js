const {
  getTruck,
  getTrucksCreatedByUser,
  addTruck,
  updateTruckType,
  updateTruckStatus,
  assignTruckToUser,
  unassignTruck,
  deleteTruckById,
  getAssignedTruckToUser,
  checkTruckToBeAssigned,
} = require("../dbcontroller/truckDBController");
const createResponseMessage = require("../private/createResponseMessage");
const { defineTruck } = require("../private/definingRequiredTruckType");
const assignController = require("./assignController");

module.exports.getTrucks = async (request, response, next) => {
  const user = request.user;
  try {
    const trucks = await getTrucksCreatedByUser(user._id);
    const responseData = trucks.map((el) => {
      return {
        _id: el._id,
        created_by: el.created_by,
        assigned_to: el.assigned_to,
        type: el.type,
        status: el.status,
        created_date: el.created_date,
      };
    });
    return response.status(200).json({
      trucks: responseData,
    });
  } catch (e) {
    return next(e);
  }
};

module.exports.addTruck = async (request, response, next) => {
  const user = request.user;
  const type = request.body.type;
  if (!type) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const result = await addTruck(user._id, type);
    if (result.created_date) {
      return response
        .status(200)
        .json(createResponseMessage("Truck created successfully"));
    } else {
      return response
        .status(400)
        .json(createResponseMessage("Truck creation failed"));
    }
  } catch (e) {
    return next(e);
  }
};

module.exports.getTruckById = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  if (!id || id.length == 0) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const truck = await getTruck(id, user._id);
    return response.status(200).json({
      truck: {
        _id: truck._id,
        created_by: truck.created_by,
        assigned_to: truck.assigned_to,
        type: truck.type,
        status: truck.status,
        created_date: truck.created_date,
      },
    });
  } catch (e) {
    return next(e);
  }
};

module.exports.updateTruck = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  const type = request.body.type;
  if (!type || !id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const isTruckAssigned = await checkTruckToBeAssigned(user._id);
    if (isTruckAssigned) {
      return response
        .status(400)
        .json(createResponseMessage("Assigned truck can't be updated"));
    } else {
      const result = await updateTruckType(id, type, user._id);
      return response
        .status(200)
        .json(createResponseMessage("Truck details changed successfully"));
    }
  } catch (e) {
    return next(e);
  }
};

module.exports.deleteTruck = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }

  try {
    const isTruckAssigned = await checkTruckToBeAssigned(user._id);
    if (isTruckAssigned) {
      return response
        .status(400)
        .json(createResponseMessage("Assigned truck can't be deleted"));
    } else {
      const result = await deleteTruckById(id, user._id);
      if (result.n >= 1) {
        return response
          .status(200)
          .json(createResponseMessage("Truck deleted successfully"));
      } else {
        return response
          .status(400)
          .json(createResponseMessage("Truck deleting failed"));
      }
    }
  } catch (e) {
    return next(e);
  }
};

module.exports.assignTruck = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }

  try {
    await unassignTruck(user._id);
    const result = await assignTruckToUser(user._id, id);
    if (result.assigned_to) {
      response
        .status(200)
        .json(createResponseMessage("Truck assigned successfully"));
    } else {
      response
        .status(400)
        .json(createResponseMessage("Truck assignition failed "));
    }

    await assignController();
    return;
  } catch (e) {
    return next(e);
  }
};
