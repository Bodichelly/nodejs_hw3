const {
  getDriversLoads,
  getShippersLoads,
  addLoadToUser,
  getDriverActiveLoad,
  iterateLoadToNextState,
  getLoadById,
  updateUserLoadById,
  deleteUserLoad,
  updateUserLoadStatus,
  assignDriverToLoad,
} = require("../dbcontroller/loadDBController");

const {
  findFreeTrucks,
  getTruck,
  getAssignedTruckToUser,
  updateTruckStatus,
} = require("../dbcontroller/truckDBController");
const load = require("../models/load");
const createResponseMessage = require("../private/createResponseMessage");

const { defineTruck } = require("../private/definingRequiredTruckType");
const assignController = require("./assignController");
const { assignTruck, updateTruck } = require("./truckController");

module.exports.getLoads = async (request, response, next) => {
  const user = request.user;
  const { status, limit, offset } = request.query;
  try {
    let loads;
    if (user.role == "DRIVER") {
      loads = await getDriversLoads(user._id, status, limit, offset);
    } else {
      loads = await getShippersLoads(user._id, status, limit, offset);
    }
    const responceFormatedData = loads.map((el) => {
      return {
        _id: el._id,
        created_by: el.created_by,
        assigned_to: el.assigned_to,
        status: el.status,
        state: el.state,
        name: el.name,
        payload: el.payload,
        pickup_address: el.pickup_address,
        delivery_address: el.delivery_address,
        dimensions: el.dimensions,
        logs: el.logs,
        created_date: el.created_date,
      };
    });
    return response.status(200).json({
      loads: responceFormatedData,
    });
  } catch (e) {
    return next(e);
  }
};
module.exports.addLoad = async (request, response, next) => {
  const user = request.user;
  let loadData;
  try {
    loadData = {
      name: request.body.name,
      payload: request.body.payload,
      pickup_address: request.body.pickup_address,
      delivery_address: request.body.delivery_address,
      dimensions: {
        width: request.body.dimensions.width,
        length: request.body.dimensions.length,
        height: request.body.dimensions.height,
      },
    };
  } catch (e) {
    return response
      .status(400)
      .json(
        createResponseMessage(
          "Query data is inappropriate (some fields are undefined)"
        )
      );
  }
  try {
    const result = await addLoadToUser(
      user._id,
      loadData.name,
      loadData.payload,
      loadData.pickup_address,
      loadData.delivery_address,
      loadData.dimensions
    );
    if (result.created_date) {
      return response
        .status(200)
        .json(createResponseMessage("Load created successfully"));
    } else {
      return response
        .status(400)
        .json(createResponseMessage("Load creation failed"));
    }
  } catch (e) {
    return next(e);
  }
};
module.exports.getDriverActiveLoad = async (request, response, next) => {
  const user = request.user;
  try {
    const load = await getDriverActiveLoad(user._id);
    response.status(200).json({ load: load });
  } catch (e) {
    return next(e);
  }
};
module.exports.updateLoadState = async (request, response, next) => {
  const user = request.user;
  try {
    const message = await iterateLoadToNextState(user._id);
    const truck = await getAssignedTruckToUser(user._id);
    if (message == "Arrived to Pick Up") {
      await updateTruckStatus(truck._id, "OL", user._id);
    } else if (message == "En route to delivery") {
      await updateTruckStatus(truck._id, "IS", user._id);
    } else if (message == "Arrived to delivery") {
      await updateTruckStatus(truck._id, "OS", user._id);
      await assignController();
    }
    return response
      .status(200)
      .json(createResponseMessage(`Load state changed to '${message}'`));
  } catch (e) {
    return next(e);
  }
};
module.exports.getUsersLoadById = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const load = await getLoadById(id);
    return response.status(200).json({ load: load });
  } catch (e) {
    return next(e);
  }
};
module.exports.updateShipperLoadById = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  let loadData = {};
  if (request.body.name) {
    loadData.name = request.body.name;
  }
  if (request.body.payload) {
    loadData.payload = parseInt(request.body.payload);
  }
  if (request.body.pickup_address) {
    loadData.pickup_address = request.body.pickup_address;
  }
  if (request.body.delivery_address) {
    loadData.delivery_address = request.body.delivery_address;
  }
  if (request.body.dimensions) {
    if (
      request.body.dimensions.width ||
      request.body.dimensions.length ||
      request.body.dimensions.height
    ) {
      loadData.dimensions = {};
      if (request.body.dimensions.width) {
        loadData.dimensions.width = parseInt(request.body.dimensions.width);
      }
      if (request.body.dimensions.length) {
        loadData.dimensions.length = parseInt(request.body.dimensions.length);
      }
      if (request.body.dimensions.height) {
        loadData.dimensions.height = parseInt(request.body.dimensions.height);
      }
    }
  }
  try {
    await updateUserLoadById(user._id, id, loadData);
    return response
      .status(200)
      .json(createResponseMessage("Load details changed successfully"));
  } catch (e) {
    return next(e);
  }
};
module.exports.deleteShipperLoadById = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const result = await deleteUserLoad(user._id, id);
    if (result.n >= 1) {
      return response
        .status(200)
        .json(createResponseMessage("Load deleted successfully"));
    } else {
      return response
        .status(400)
        .json(createResponseMessage("Load deleting failed"));
    }
  } catch (e) {
    return next(e);
  }
};
module.exports.postShipperLoad = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const load = await getLoadById(id);
    if (load.status !== "NEW") {
      return response
        .status(400)
        .json(
          createResponseMessage(`Load woth id: ${id} is already processed`)
        );
    }
    await updateUserLoadStatus(id, "POSTED");
    if (
      !load.dimensions ||
      !load.dimensions.width ||
      !load.dimensions.length ||
      !load.dimensions.height
    ) {
      return response
        .status(500)
        .json(createResponseMessage(`Internal data lose`));
    }
    // const truckType = defineTruck(load.dimensions);
    const trucks = await findFreeTrucks();
    if (!trucks || trucks.length == 0) {
      return response.status(200).json({
        message: "Load posted successfully",
        driver_found: false,
      });
    }
    await assignDriverToLoad(trucks[0].assigned_to, id);
    return response.status(200).json({
      message: "Load posted successfully",
      driver_found: true,
    });
  } catch (e) {
    return next(e);
  }
};
module.exports.getLoadShippingInfoById = async (request, response, next) => {
  const user = request.user;
  const { id } = request.params;
  if (!id) {
    return response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
  }
  try {
    const load = await getLoadById(id);
    const truck = await getAssignedTruckToUser(load.assigned_to);
    return response.status(200).json({
      load: {
        _id: load._id,
        created_by: load.created_by,
        assigned_to: load.assigned_to,
        status: load.status,
        state: load.state,
        name: load.name,
        payload: load.payload,
        pickup_address: load.pickup_address,
        delivery_address: load.delivery_address,
        dimensions: load.dimensions,
        logs: load.logs,
        created_date: load.created_date,
      },
      truck: {
        _id: truck._id,
        created_by: truck.created_by,
        assigned_to: truck.assigned_to,
        type: truck.type,
        status: truck.status,
        created_date: truck.created_date,
      },
    });
  } catch (e) {
    return next(e);
  }
};
