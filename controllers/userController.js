const {
  deleteUser,
  updateUserPassword,
  getUserByEmailAndPassword,
} = require("../dbcontroller/userDBController");
const createResponseMessage = require("../private/createResponseMessage");
const { getPasswordHashWithDefaultSalt } = require("../private/hash");

module.exports.getUser = async (request, response, next) => {
  const { email, password } = request.user;
  if (!email || !password) {
    response.status(400).json(createResponseMessage("Invalid query data"));
    return;
  }
  try {
    const user = await getUserByEmailAndPassword(email, password);
    const userResponseFormat = {
      _id: user._id,
      email: user.email,
      created_date: user.created_date,
    };

    response.status(200).json(userResponseFormat);
    return;
  } catch (e) {
    return next(e);
  }
};

module.exports.deleteUser = async (request, response, next) => {
  const { email, password } = request.user;
  if (!email || !password) {
    response.status(400).json(createResponseMessage("Invalid query data"));
    return;
  }
  try {
    const user = await getUserByEmailAndPassword(email, password);
    const result = await deleteUser(user._id);
    if (result.n >= 1) {
      return response
        .status(200)
        .json(createResponseMessage("Profile deleted successfully"));
    } else {
      return response
        .status(400)
        .json(createResponseMessage("Profile deleting failed"));
    }
  } catch (e) {
    return next(e);
  }
};

module.exports.patchUser = async (request, response, next) => {
  const { oldPassword, newPassword } = request.body;
  const { email } = request.user;
  if (!email || !oldPassword || !newPassword) {
    response
      .status(400)
      .json(createResponseMessage("Query data is inappropriate"));
    return;
  }
  const passwordHash = getPasswordHashWithDefaultSalt(newPassword);
  const oldPasswordHash = getPasswordHashWithDefaultSalt(oldPassword);
  try {
    const user = await getUserByEmailAndPassword(email, oldPasswordHash);
    const result = await updateUserPassword(user._id, passwordHash);
    return response.status(200).json(createResponseMessage("Success"));
  } catch (e) {
    return next(e);
  }
};
