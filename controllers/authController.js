const jwt = require("jsonwebtoken");
const {
  addUser,
  getUserByEmailAndPassword,
  getUserByEmail,
} = require("../dbcontroller/userDBController");

const { secret } = require("../config/auth");
const { request, response } = require("express");

const { getPasswordHashWithDefaultSalt } = require("../private/hash");
const createResponseMessage = require("../private/createResponseMessage");

module.exports.register = async (request, response, next) => {
  const { email, password, role } = request.body;
  if (!email || !password || !role) {
    return response
      .status(400)
      .json(createResponseMessage("Invalid query data"));
  }

  try {
    const passwordHash = getPasswordHashWithDefaultSalt(password);
    await addUser(email, passwordHash, role);
    return response
      .status(200)
      .json(createResponseMessage("Profile created successfully"));
  } catch (e) {
    return next(e);
  }
};

module.exports.login = async (request, response, next) => {
  const { email, password } = request.body;
  if (!email || !password) {
    response
      .status(400)
      .json(createResponseMessage("Invalid email or password"));
    return;
  }

  try {
    const passwordHash = getPasswordHashWithDefaultSalt(password);
    const user = await getUserByEmailAndPassword(email, passwordHash);
    if (!user) {
      return response
        .status(400)
        .json(
          createResponseMessage("No user with such email and password found")
        );
    } else {
      const jwtData = {
        id: user._id,
        email: user.email,
      };
      return response
        .status(200)
        .json({ jwt_token: jwt.sign(JSON.stringify(jwtData), secret) });
    }
  } catch (e) {
    return next(e);
  }
};

module.exports.forgot_password = async (request, response, next) => {
  //   response.setHeader("Content-Type", "application/json");
  //   const { email } = request.body;
  //   if (!email || email.length == 0) {
  //     response.status(400).json({ message: "Invalid email" });
  //     return;
  //   }
  //   try {
  //     const newPassword = Math.random().toString(36).slice(-8);
  //     const user = await getUserByEmail(email);
  //     if (!user) {
  //       response
  //         .status(400)
  //         .json({ message: "No user with such username and password found" });
  //     } else {
  //       response
  //         .status(200)
  //         .json({ jwt_token: jwt.sign(JSON.stringify(user), secret) });
  //     }
  //   } catch (e) {
  //     response.status(e.status).json({ message: e.message });
  //   }
  return next({ status: 500, message: "This serivce disabled" });
};
