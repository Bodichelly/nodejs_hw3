const {
  getAllPostedLoads,
  assignDriverToLoad,
} = require("../dbcontroller/loadDBController");
const {
  findFreeTrucks,
  assignTruckToUser,
} = require("../dbcontroller/truckDBController");

module.exports = async () => {
  try {
    const loads = await getAllPostedLoads();
    if (!loads || loads.length === 0) {
      return false;
    }
    const trucks = await findFreeTrucks();
    if (!trucks || trucks.length === 0) {
      return false;
    }
    let num = 0;
    for (let i = 0; i < trucks.length; i++) {
      const currentLoad = loads.shift();
      const flag = await assignDriverToLoad(
        trucks[i].assigned_to,
        currentLoad._id
      );
      if (flag) {
        num++;
      }
    }

    return num !== 0;
  } catch (e) {
    return false;
  }
};
