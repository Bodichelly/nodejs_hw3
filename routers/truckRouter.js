const express = require("express");

const authMiddleware = require("../middlewares/authMiddleware");
const driverMiddleware = require("../middlewares/driverMiddleware");

const router = express.Router();

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require("../controllers/truckController");
const { getTruck } = require("../dbcontroller/truckDBController");

router.get("/trucks", authMiddleware, driverMiddleware, getTrucks);
router.post("/trucks", authMiddleware, driverMiddleware, addTruck);
router.get("/trucks/:id", authMiddleware, driverMiddleware, getTruckById);
router.put("/trucks/:id", authMiddleware, driverMiddleware, updateTruck);
router.delete("/trucks/:id", authMiddleware, driverMiddleware, deleteTruck);
router.post(
  "/trucks/:id/assign",
  authMiddleware,
  driverMiddleware,
  assignTruck
);

module.exports = router;
