const express = require("express");

const authMiddleware = require("../middlewares/authMiddleware");

const router = express.Router();

const {
  getUser,
  deleteUser,
  patchUser,
} = require("../controllers/userController");

router.get("/users/me", authMiddleware, getUser);

router.delete("/users/me", authMiddleware, deleteUser);

router.patch("/users/me/password", authMiddleware, patchUser);

module.exports = router;
