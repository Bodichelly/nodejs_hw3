const express = require("express");

const authMiddleware = require("../middlewares/authMiddleware");
const driverMiddleware = require("../middlewares/driverMiddleware");
const shipperMiddleware = require("../middlewares/shipperMiddleware");

const router = express.Router();

const {
  getLoads,
  addLoad,
  getDriverActiveLoad,
  updateLoadState,
  getUsersLoadById,
  updateShipperLoadById,
  deleteShipperLoadById,
  postShipperLoad,
  getLoadShippingInfoById,
} = require("../controllers/loadController");

router.get("/loads", authMiddleware, getLoads);
router.post("/loads", authMiddleware, shipperMiddleware, addLoad);
router.get(
  "/loads/active",
  authMiddleware,
  driverMiddleware,
  getDriverActiveLoad
);
router.patch(
  "/loads/active/state",
  authMiddleware,
  driverMiddleware,
  updateLoadState
);
router.get("/loads/:id", authMiddleware, getUsersLoadById);
router.put("/loads/:id", authMiddleware, updateShipperLoadById);
router.delete("/loads/:id", authMiddleware, deleteShipperLoadById);
router.post("/loads/:id/post", authMiddleware, postShipperLoad);
router.get("/loads/:id/shipping_info", authMiddleware, getLoadShippingInfoById);

module.exports = router;
