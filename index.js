const express = require("express");
const mongoose = require("mongoose");
const app = express();

const dbCheckMiddleware = require("./middlewares/dbCheckMiddleware");

let isConnected = false;

const { port } = require("./config/server");
const dbConfig = require("./config/database");

const userRouter = require("./routers/userRouter");
const authRouter = require("./routers/authRouter");
const loadRoute = require("./routers/loadRouter");
const truckRoute = require("./routers/truckRouter");
const { request, response } = require("express");
const errorHandler = require("./middlewares/errorHandler");

const dbConnectString = `mongodb+srv://${dbConfig.username}:${dbConfig.password}@nodejshw3uberdelivery.xnduq.mongodb.net/${dbConfig.databaseName}?retryWrites=true&w=majority`;
mongoose.connect(dbConnectString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

app.use(express.json());

app.use("/api", dbCheckMiddleware, userRouter);
app.use("/api", dbCheckMiddleware, authRouter);
app.use("/api", dbCheckMiddleware, loadRoute);
app.use("/api", dbCheckMiddleware, truckRoute);
app.use("/", (request, response) => {
  response
    .status(400)
    .json({ message: "Data or query path could not be found" });
});
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server listens on ${port} port`);
});
