const Truck = require("../models/truck");
const serverErrorThrow = require("../private/serverErrorThrow");

const typesArr = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];
const statusArr = ["OS", "OL", "IS"];

module.exports.getTruck = async (id, driverId) => {
  let truck;
  try {
    truck = await Truck.findById(id).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!truck) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return truck;
};
module.exports.getTrucksCreatedByUser = async (id) => {
  let trucks;
  const query = {
    created_by: id,
  };

  try {
    trucks = await Truck.find(query).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!trucks) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  if (trucks.length == 0) {
    throw {
      status: 200,
      message: "No content",
    };
  }
  return trucks;
};

module.exports.checkTruckToBeAssigned = async (driverId) => {
  const query = {
    created_by: driverId,
    assigned_to: driverId,
  };
  try {
    const number = await Truck.count(query);
    if (number === 1) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.getAssignedTruckToUser = async (driverId) => {
  let truck;
  const query = {
    created_by: driverId,
    assigned_to: driverId,
  };
  try {
    truck = await Truck.find(query).lean().exec();
  } catch (e) {
    throw {
      status: 400,
      message: JSON.stringify(e) || e + "",
    };
  }

  if (!truck) {
    serverErrorThrow(e);
  }
  return truck;
};

module.exports.deleteTruckById = async (id, driverId) => {
  const query = {
    _id: id,
    created_by: driverId,
  };
  try {
    return await Truck.deleteOne(query);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.addTruck = async (userId, type) => {
  const createdDate = new Date().toISOString();
  if (!typesArr.includes(type.toUpperCase())) {
    throw {
      status: 400,
      message: "Inappropriate truck type",
    };
  }
  const truck = new Truck({
    created_by: userId,

    type: type.toUpperCase(),
    status: "OS",
    created_date: createdDate,
  });
  try {
    return await truck.save();
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.updateTruckType = async (_id, type, driverId) => {
  if (!typesArr.includes(type.toUpperCase())) {
    throw {
      status: 400,
      message: "Inappropriae input data",
    };
  }
  const query = {
    _id: _id,
    created_by: driverId,
  };
  const update = {
    type: type.toUpperCase(),
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await Truck.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.updateTruckStatus = async (_id, status, driverId) => {
  const query = {
    _id: _id,
    created_by: driverId,
  };
  const update = {
    status: statusArr.includes(status.toUpperCase()),
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await Truck.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.assignTruckToUser = async (driverId, truckId) => {
  const query = {
    _id: truckId,
    created_by: driverId,
  };
  const update = {
    assigned_to: driverId,
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await Truck.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.unassignTruck = async (driverId) => {
  const query = {
    assigned_to: driverId,
  };
  const update = {
    assigned_to: null,
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await Truck.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.findFreeTrucks = async () => {
  const query = {
    // type: type.toUpperCase(),
    status: statusArr[0],
    assigned_to: { $ne: null },
  };
  try {
    const trucks = await Truck.find(query).lean().exec();
    return trucks;
  } catch (e) {
    return [];
  }
};
