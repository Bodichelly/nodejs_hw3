const User = require("../models/user");
const serverErrorThrow = require("../private/serverErrorThrow");

module.exports.getUser = async (id) => {
  let user;
  try {
    user = await User.findById(id).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!user) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return user;
};
module.exports.getUserByEmailAndPassword = async (email, password) => {
  const query = {
    email: email,
    password: password,
  };
  try {
    const user = await User.findOne(query).lean().exec();
    if (!user) {
      throw {
        status: 400,
        message: "Data not found",
      };
    }
    return user;
  } catch (e) {
    serverErrorThrow(e);
  }
};
module.exports.deleteUser = async (_id) => {
  const query = {
    _id: _id,
  };
  try {
    return await User.deleteOne(query);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.addUser = async (email, password, userRole) => {
  const createdDate = new Date().toISOString();
  if (!createdDate) {
    throw {
      status: 500,
      message: "Internal data lose",
    };
  }
  const user = new User({
    email: email,
    password: password,
    created_date: createdDate,
    role: userRole,
  });
  try {
    return await user.save();
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.updateUserPassword = async (_id, password) => {
  const query = {
    _id: _id,
  };
  const update = {
    password: password,
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await User.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.updateUserRole = async (_id, role) => {
  const query = {
    _id: _id,
  };
  const update = {
    role: role,
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await User.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.getUserByEmail = async (email) => {
  const query = {
    email: email,
  };
  let user;
  try {
    user = await User.findOne(query).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!user) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return user;
};
