const { updateLoadState } = require("../controllers/loadController");
const { syncIndexes, off } = require("../models/load");
const Load = require("../models/load");
const user = require("../models/user");
const serverErrorThrow = require("../private/serverErrorThrow");
const {
  updateTruckStatus,
  getTruck,
  getAssignedTruckToUser,
} = require("./truckDBController");

const statesArr = [
  "En route to Pick Up",
  "Arrived to Pick Up",
  "En route to delivery",
  "Arrived to delivery",
];
const statusArr = ["NEW", "POSTED", "ASSIGNED", "SHIPPED"];

module.exports.getShippersLoads = async (
  userId,
  status,
  limit = 10,
  offset = 0
) => {
  let loads;
  let query = {
    created_by: userId,
  };

  if (status) {
    query.status = status;
  }
  try {
    loads = await Load.find(query)
      .skip(parseInt(offset))
      .limit(parseInt(limit))
      .lean()
      .exec();
  } catch (e) {
    serverErrorThrow(e);
  }
  if (!loads) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  if (loads.length == 0) {
    throw {
      status: 200,
      message: "No content",
    };
  }
  return loads;
};

module.exports.getDriversLoads = async (
  driverId,
  status,
  limit = 10,
  offset = 0
) => {
  let loads;
  let query = {
    assigned_to: driverId,
  };
  if (status && (status === statusArr[2] || status === statusArr[3])) {
    query.status = status;
  }
  try {
    loads = await Load.find(query)
      .skip(parseInt(offset))
      .limit(parseInt(limit))
      .lean()
      .exec();
  } catch (e) {
    serverErrorThrow(e);
  }
  if (!loads) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  if (loads.length == 0) {
    throw {
      status: 200,
      message: "No content",
    };
  }

  return loads;
};

module.exports.addLoadToUser = async (
  userId,
  name,
  payload,
  pickup_address,
  delivery_address,
  dimensions
) => {
  const createdDate = new Date().toISOString();
  const load = new Load({
    created_by: userId,
    assigned_to: null,
    status: statusArr[0],
    state: null,
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: {
      width: dimensions.width,
      length: dimensions.length,
      height: dimensions.height,
    },
    created_date: createdDate,
  });
  try {
    return await load.save();
  } catch (e) {
    serverErrorThrow(e);
  }
};

const getDriverActiveLoad = async (driverId) => {
  let load;
  const query = {
    assigned_to: driverId,
    status: statusArr[2],
  };
  try {
    load = await Load.findOne(query).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }
  if (!load) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return load;
};

module.exports.iterateLoadToNextState = async (driverId) => {
  try {
    const load = await getDriverActiveLoad(driverId);
    const index = statesArr.indexOf(load.state);
    const query = {
      _id: load._id,
      assigned_to: driverId,
    };
    const update = {
      state: statesArr[index + 1] || statesArr[3],
    };
    const options = {
      returnOriginal: false,
    };
    await Load.findOneAndUpdate(query, update, options);
    await addLogsToLoad(load._id, `Load state changed to '${update.state}'`);
    if (update.state == statesArr[3]) {
      await this.updateUserLoadStatus(load._id, statusArr[3]);
      const truck = await getAssignedTruckToUser(load.assigned_to);
      await updateTruckStatus(truck._id, "OS", load.assigned_to);
    }
    return update.state;
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.getLoadById = async (loadId) => {
  let load;
  try {
    load = await Load.findById(loadId).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!load) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return load;
};

module.exports.getUserLoadById = async (userId, loadId) => {
  let load;
  const query = {
    _id: loadId,
    created_by: userId,
  };
  try {
    load = await Load.find(query).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }

  if (!load) {
    throw {
      status: 400,
      message: "Data not found",
    };
  }
  return load;
};

const updateUserLoadStatus = async (loadId, status) => {
  if (!statusArr.includes(status.toUpperCase())) {
    throw {
      status: 400,
      message: "Inappropriate input data",
    };
  }
  const query = {
    _id: loadId,
  };
  const update = {
    status: status,
  };
  const options = {
    returnOriginal: false,
  };
  try {
    await Load.findOneAndUpdate(query, update, options);
    await addLogsToLoad(loadId, `Load status changed to '${status}'`);
    return `Load state changed to '${update.status}'`;
  } catch (e) {
    serverErrorThrow(e);
  }
};
module.exports.updateUserLoadById = async (userId, loadId, newLoadData) => {
  const query = {
    _id: loadId,
    created_by: userId,
    status: statusArr[0],
  };

  let update = newLoadData;

  const options = {
    returnOriginal: false,
  };
  try {
    const result = await Load.findOneAndUpdate(query, update, options);
    await addLogsToLoad(loadId, `Load info updated`);
    return result;
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.deleteUserLoad = async (userId, loadId) => {
  const query = {
    _id: loadId,
    created_by: userId,
    status: statusArr[0],
  };
  try {
    return await Load.deleteOne(query);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.assignDriverToLoad = async (driverId, loadId) => {
  const query = {
    _id: loadId,
  };
  const update = {
    assigned_to: driverId,
    status: statusArr[2],
  };
  const options = {
    returnOriginal: false,
  };
  try {
    const result = await Load.findOneAndUpdate(query, update, options);
    await addLogsToLoad(loadId, `Load assigned to driver with id ${driverId}`);
    return result;
  } catch (e) {
    serverErrorThrow(e);
  }
};

const addLogsToLoad = async (loadId, message) => {
  const time = new Date().toISOString();
  const query = {
    _id: loadId,
  };
  const update = {
    $push: {
      logs: {
        message: message,
        time: time,
      },
    },
  };
  const options = {
    returnOriginal: false,
  };
  try {
    return await Load.findOneAndUpdate(query, update, options);
  } catch (e) {
    serverErrorThrow(e);
  }
};

module.exports.getAllPostedLoads = async () => {
  let loads;
  let query = {
    assigned_to: null,
    status: statusArr[1],
  };

  try {
    loads = await Load.find(query).lean().exec();
  } catch (e) {
    serverErrorThrow(e);
  }
  return loads;
};

module.exports.addLogsToLoad = addLogsToLoad;
module.exports.updateUserLoadStatus = updateUserLoadStatus;
module.exports.getDriverActiveLoad = getDriverActiveLoad;
